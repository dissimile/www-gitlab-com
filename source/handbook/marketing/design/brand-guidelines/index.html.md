---
layout: markdown_page
title: "Brand Guidelines"
---
# Welcome to the Brand Guidelines Handbook

[Up one level to the Design Handbook](/handbook/marketing/design)    

## On this page
* [Introduction](#introduction)
* [The Tanuki](#tanuki)
* [Brand Police](#brandPolice)
* [Colors](#colors)
* [Typography](#typography)
* [Wordmark & Usage](#wordmark)
* [Logo & Usage](#logo)
* [Iconography](#icons)


## Introduction<a name="introduction"></a>

Welcome to the Brand Guidelines handbook, here you will find everything you need to know about the GitLab brand and it's usage. To download the GitLab logo (in various formats and file types) check out our [Press page](https://about.gitlab.com/press/).

**Have ideas or suggestions?** Create an issue in the [GitLab Artwork](https://gitlab.com/gitlab-com/gitlab-artwork/issues) issue tracker.

## The Tanuki<a name="tanuki"></a>

The [tanuki](https://en.wikipedia.org/wiki/Japanese_raccoon_dog) is a very smart animal that works together in a group to achieve a common goal. We feel this symbolism embodies GitLab's [vision](https://about.gitlab.com/about/#vision) that everyone can contribute, our [values](https://about.gitlab.com/about/#values), and our [open source stewardship](https://about.gitlab.com/2016/01/11/being-a-good-open-source-steward/).

## Brand Police<a name="brandPolice"></a>

Occasionally the [old GitLab logo](https://gitlab.com/gitlab-com/gitlab-artwork/blob/master/archive/logo/fox.png) is still in use on partner websites, diagrams or images, and within documentation. If you come across our old logo in use, please bring it to our attention by created an issue in the [GitLab Artwork](https://gitlab.com/gitlab-com/gitlab-artwork/issues) issue tracker. Please include a link and screenshot (if possible) in the description of the issue.
